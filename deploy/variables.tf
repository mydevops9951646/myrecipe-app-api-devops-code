variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "myrecipe-app-api-devops"
}

variable "contact" {
  default = "email@myemail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "myrecipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "614876196400.dkr.ecr.us-east-1.amazonaws.com/myrecipe-app-api-devops-code:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "614876196400.dkr.ecr.us-east-1.amazonaws.com/myrecipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}


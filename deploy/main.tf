terraform {
  backend "s3" {
    bucket         = "myrecipe-app-api-tfstate"
    key            = "myrecipe-app-tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "myrecipe-app-api-devops-tf-state-lock"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "2.54.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owoner      = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}